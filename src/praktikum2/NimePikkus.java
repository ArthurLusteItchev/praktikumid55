package praktikum2;

import lib.TextIO;

public class NimePikkus {

	public static void main(String[] args) {
		
		System.out.println("Sisesta nimi");
		String nimi = TextIO.getlnString();
		String nimiAinultT2hed = nimi.replaceAll(" ", "").replaceAll("-", "");
		System.out.println(nimiAinultT2hed);
		int nimePikkus = nimiAinultT2hed.length();
		System.out.println(nimePikkus);

	}

}
