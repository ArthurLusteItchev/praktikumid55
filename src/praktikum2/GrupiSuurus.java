package praktikum2;

import lib.TextIO;

public class GrupiSuurus {

	public static void main(String[] args) {

		System.out.println("Palun sisesta inimeste arv");
		int inimesteArv = TextIO.getlnInt();

		System.out.println("Palun sisesta grupi suurus arv");
		int grupiSuurus = TextIO.getlnInt();

		int gruppideArv = inimesteArv / grupiSuurus;
		System.out.println("Saame moodutada " + gruppideArv + " gruppi");
		
		int j22k = inimesteArv % grupiSuurus;
		System.out.println("Üle jääb " + j22k + " inimest");
	}

}
