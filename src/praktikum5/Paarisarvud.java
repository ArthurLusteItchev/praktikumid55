package praktikum5;

public class Paarisarvud {

	public static void main(String[] args) {
		
		// i += 2 // i = i + 2
		// i -= 2 // i = i - 2
		
		for (int i = 0; i <= 10; i += 2) {
			System.out.println(i);
		}
		
		for (int i = 0; i <= 10; i++) {
			if (i % 2 == 0) {
				System.out.println(i);							
			}
		}
	}
}
