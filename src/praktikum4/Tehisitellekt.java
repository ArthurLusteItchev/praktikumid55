package praktikum4;

import lib.TextIO;

public class Tehisitellekt {

	public static void main(String[] args) {
	
		System.out.println("Palun sisesta kaks vanust");
		
		int vanus1 = kysiVanus();
		int vanus2 = kysiVanus();
		int vanusteVahe = Math.abs(vanus1 - vanus2);
 
		if (vanusteVahe > 10) {
			System.out.println("Mingi jama!");
		} else if (vanusteVahe > 5) {
			System.out.println("Ikka ei sobi.");
		} else {
			System.out.println("Sobib!");
		}
		
	}

	private static int kysiVanus() {
		int vanus;
		while (true) {
			vanus = TextIO.getlnInt();
			if (vanus < 0) {
				System.out.println("Vanus ei saa olla negatiivne, sisesta uuesti");
			} else {
				break;
			}
		}
		return 0;
	}
	
}
